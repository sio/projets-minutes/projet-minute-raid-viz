CREATE TABLE activite
(
 code character varying(5) NOT NULL,
 libelle character varying(50),
 CONSTRAINT pk_activite PRIMARY KEY (code)
);
CREATE TABLE raid
(
 code character varying(6) NOT NULL,
 nom character varying(50),
 date_depart character varying(10),
 ville character varying(50),
 region character varying(50),
 nb_maxi_par_equipe integer,
 montant_inscription numeric(8,2),
 nb_femmes integer,
 duree_maxi integer,
 age_minimum integer,
 CONSTRAINT pk_raid PRIMARY KEY (code)
);
CREATE TABLE activite_raid
(
 code_raid character varying(6) NOT NULL,
 code_activite character varying(5) NOT NULL,
 CONSTRAINT pk_activite_raid PRIMARY KEY (code_raid, code_activite),
 CONSTRAINT activite_raid_code_activite_fkey FOREIGN KEY (code_activite)
  REFERENCES activite (code) MATCH SIMPLE
  ON UPDATE NO ACTION
  ON DELETE NO ACTION,
 CONSTRAINT activite_raid_code_raid_fkey FOREIGN KEY (code_raid)
  REFERENCES raid (code) MATCH SIMPLE
  ON UPDATE NO ACTION
  ON DELETE NO ACTION
);
CREATE TABLE departement
(
 numero integer NOT NULL,
 nom character varying(30),
 CONSTRAINT pk_departement PRIMARY KEY (numero)
);
CREATE TABLE traverser
(
 code_raid character varying(6) NOT NULL,
 numero_dept integer NOT NULL,
 CONSTRAINT pk_traverser PRIMARY KEY (code_raid, numero_dept),
 CONSTRAINT traverser_code_raid_fkey FOREIGN KEY (code_raid)
  REFERENCES raid (code) MATCH SIMPLE
  ON UPDATE NO ACTION
  ON DELETE NO ACTION,
   CONSTRAINT traverser_code_dept_fkey FOREIGN KEY (numero_dept)
  REFERENCES departement (numero) MATCH SIMPLE
  ON UPDATE NO ACTION
  ON DELETE NO ACTION
);
